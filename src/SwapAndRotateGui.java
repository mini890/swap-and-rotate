import java.awt.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

/**
 * A GUI that use the SwapAndRotate class to encode/decode messages
 * In this GUI we cannot prompt for a String to decode as the swapped and rotated bits
 * in byte may generate characters that may (most probably) not be displayable
 *
 * The GUI display the binary form of each letter in the message
 * their coded representaion for SWAP, ROTATE and BOTH
 * The bottom of the frame shows if each key SWAP and ROTATE are valid
 */
public class SwapAndRotateGui extends JFrame {

    private static final long serialVersionUID = 1L;

    // the SwapAndRotate class to encode/decode
    private SwapAndRotate war;

    // the keys to use
    private JTextField keySwapText, keyRotateText;
    // The message to encode
    private JTextField clearTextIn;

    // The message saying if the keys are valid
    private boolean keySwapValid, keyRotateValid;
    private JLabel swapValidLabel, rotateValidLabel;

    // the original message
    private char[] msgChar = new char[0];
    // the decoded messages
    private char[] swapEncoded, rotateEncoded, bothEncoded;
    // the encoded messages
    private char[] swapDecoded, rotateDecoded, bothDecoded;

    // The JTable shown in the CENTER region
    private JTable table;
    private MyModel myModel;
    // Its panel
    private JPanel centerPanel;
    // it's headers
    private static final String[] header = {"Orig msg", "Swappped crypted", "Rotated crypted", "Both crypted",
            "Swap decripted", "Rotate decrypted", "Both decrypted"};
    // mnemonic for more descriptive values in the AbstractModel
    private static final int ORIG = 0, SWAPPED = 1, ROTATED = 2, BOTH = 3,
            SWAP_BACK = 4, ROTATE_BACK = 5, BOTH_BACK = 6;

    /**
     * Constructor
     */
    SwapAndRotateGui() {
        super("SwapAndRotate encoding/decoding by Mini890");
        // we will use a BorderLayout to store our GUI component
        setLayout(new BorderLayout());

        // the SwapAndRotate object init the keys to ""
        war = new SwapAndRotate("", "");

        // the Listener when one of the key changes
        DocumentListener dc = new KeyListener();
        // The NORTH region will contain a JPanel where the keys and the message can be entererd
        JPanel north = new JPanel(new GridLayout(7,1, 2, 2));
        // the SWAP key
        north.add(createCenteredLabel("The SWAP key composed of pairs of bits to swap like 071625"));
        keySwapText = new JTextField(50);
        keySwapText.getDocument().addDocumentListener(dc);
        north.add(keySwapText);
        // the ROTATE key
        north.add(createCenteredLabel("The ROTATE key composed of numbers of bits to rotate like 12,-34,92,18"));
        keyRotateText = new JTextField(50);
        keyRotateText.getDocument().addDocumentListener(dc);
        north.add(keyRotateText);
        // the message
        north.add(createCenteredLabel("Enter the message to decode here"));
        clearTextIn = new JTextField(50);
        clearTextIn.getDocument().addDocumentListener(new ClearListener());
        north.add(clearTextIn);
        // a gap
        north.add(new JLabel(" "));
        // add this panel to the top of the screen
        add(north, BorderLayout.NORTH);

        // in the CENTER region of the frame we will put a JTable containing all the
        // encrypted/decripted bits
        myModel = new MyModel();
        table = new JTable(myModel);
        centerPanel = new JPanel(new GridLayout(1,1));
        centerPanel.add(new JScrollPane(table));
        add(centerPanel, BorderLayout.CENTER);


        // The SOUTH region contains 2 JLabel specifying if the keys are valid
        JPanel south = new JPanel(new GridLayout(1, 2));
        // create the 2 labels using our centered label factory
        swapValidLabel = createCenteredLabel("");
        rotateValidLabel = createCenteredLabel("");
        // we make them opaque as their foreground color changes depending if
        // the key is valid or not
        swapValidLabel.setOpaque(true);
        rotateValidLabel.setOpaque(true);
        // put them in the panel
        south.add(swapValidLabel);
        south.add(rotateValidLabel);
        updateValidLabel();
        add(south, BorderLayout.SOUTH);

        // standard operation to show the JFrame
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(30, 30, 900, 500);
        setVisible(true);
    }

    /**
     * A method to create a JLabel with foreground color Blue and with text centered
     */
    private JLabel createCenteredLabel(String text) {
        JLabel label = new JLabel(text);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setForeground(Color.BLUE);
        return label;
    }

    /**
     * One of the 2 key has changed
     */
    private void updateKeyString() {
        String keyStr;

        // update both keys from their JTextField
        keyStr = keySwapText.getText();
        // check if SWAP key is valid
        keySwapValid = SwapAndRotate.bValidateSwapKey(keyStr);
        war.setKeySwap(keyStr);

        // check if ROTATE key is valid
        keyStr = keyRotateText.getText();
        keyRotateValid = SwapAndRotate.bValidateRotateKey(keyStr);
        war.setKeyRotate(keyStr);

        // update the JLabel that says if the key are valid
        updateValidLabel();
        // set the color of the JTextTextFields displaying the key in black if the key is valid
        // in RED is the key is not valid
        if(keySwapValid)
            keySwapText.setForeground(Color.BLACK);
        else
            keySwapText.setForeground(Color.RED);
        if(keyRotateValid)
            keyRotateText.setForeground(Color.BLACK);
        else
            keyRotateText.setForeground(Color.RED);

        // update the keys in the SwapAndRotateObject
        // and update the string to encode
        updateStringToEncode();
    }

    /**
     * To update the string to be coded
     */
    private void updateStringToEncode() {
        // get the text from the JTextField
        String line = clearTextIn.getText();
        // make the char[] array out of it to be displayed in the JTable
        msgChar = line.toCharArray();
        // build the encrypted char[] for the 3 coding algorithms
        swapEncoded = war.encode(line, SwapAndRotate.SWAP);
        rotateEncoded = war.encode(line, SwapAndRotate.ROTATE);
        bothEncoded = war.encode(line, SwapAndRotate.BOTH);
        // build the decrypted char[] for the 3 coding algorithms
        swapDecoded = war.decode(new String(swapEncoded), SwapAndRotate.SWAP);
        rotateDecoded = war.decode(new String(rotateEncoded), SwapAndRotate.ROTATE);
        bothDecoded = war.decode(new String(bothEncoded), SwapAndRotate.BOTH);
        // inform the model that the table contain changed
        myModel.fireTableDataChanged();
    }


    /**
     * To update the labels saying if the keys are valid
     */
    private static final Color darkGreen = new Color(0, 120, 0);
    private void updateValidLabel() {
        // updates the text of both labels saying if the key is valid
        // update also the foregound and background colors
        // valid: WHITE and GREEN
        // no valid: YELLOW and RED
        if(keySwapValid) {
            swapValidLabel.setText("SWAP key is valid");
            swapValidLabel.setForeground(darkGreen);
            swapValidLabel.setBackground(Color.WHITE);
        }
        else {
            swapValidLabel.setText("SWAP key is NOT valid");
            swapValidLabel.setForeground(Color.RED);
            swapValidLabel.setBackground(Color.YELLOW);
        }

        if(keyRotateValid) {
            rotateValidLabel.setText("ROTATE key is valid");
            rotateValidLabel.setForeground(darkGreen);
            rotateValidLabel.setBackground(Color.WHITE);
        }
        else {
            rotateValidLabel.setText("ROTATE key is NOT valid");
            rotateValidLabel.setForeground(Color.RED);
            rotateValidLabel.setBackground(Color.YELLOW);
        }


    }
    /**
     * To start the GUI
     */
    public static void main(String[] args) {
        new SwapAndRotateGui();
    }

    /**
     * A listener to be informed whenever the JTextField of the clear text is changed
     */
    private class ClearListener implements DocumentListener {
        @Override
        public void changedUpdate(DocumentEvent arg0) {
            updateStringToEncode();
        }
        @Override
        public void insertUpdate(DocumentEvent arg0) {
            updateStringToEncode();
        }
        @Override
        public void removeUpdate(DocumentEvent arg0) {
            updateStringToEncode();
        }
    }

    /**
     * A listener to be informed whenever the JTextField of the SWAP or ROTATE key is changed
     */
    private class KeyListener implements DocumentListener {
        @Override
        public void changedUpdate(DocumentEvent arg0) {
            updateKeyString();
        }
        @Override
        public void insertUpdate(DocumentEvent arg0) {
            updateKeyString();
        }
        @Override
        public void removeUpdate(DocumentEvent arg0) {
            updateKeyString();
        }
    }

    /**
     * A class that extends AbstractTableModel to povide the binary representation
     * of every cell of the JTable in the center panel
     */
    private class MyModel extends AbstractTableModel {

        private static final long serialVersionUID = 1L;

        // the number of columns is the length of the header of these columns
        public int getColumnCount() {
            return header.length;
        }

        // name of each colum
        public String getColumnName(int column) {
            return header[column];
        }
        // return the row count
        public int getRowCount() {
            // return the length of the message in the JTextField
            return clearTextIn.getText().length();
        }

        // the JTable want's to know what to print there
        public Object getValueAt(int row, int col) {
            // validate first the row
            if(row >= msgChar.length)
                return "";
            // the CharAndBits to display
            CharAndBits cab = null;
            // the cell text
            String str;

            // depending of the column display the correct text field
            switch(col) {
                // the only one where the char preceedes the binary String
                case ORIG:
                    cab = new CharAndBits(msgChar[row]);
                    str = cab.getPrintableChar() + " - " + cab.toBinaryString();
                    return str;
                case SWAPPED:
                    if(row >= swapEncoded.length)
                        break;
                    cab = new CharAndBits(swapEncoded[row]);
                    break;
                case ROTATED:
                    if(row >= rotateEncoded.length)
                        break;
                    cab = new CharAndBits(rotateEncoded[row]);
                    break;
                case BOTH:
                    if(row >= bothEncoded.length)
                        break;
                    cab = new CharAndBits(bothEncoded[row]);
                    break;
                case SWAP_BACK:
                    if(row >= swapDecoded.length)
                        break;
                    cab = new CharAndBits(swapDecoded[row]);
                    break;
                case ROTATE_BACK:
                    if(row >= rotateDecoded.length)
                        break;
                    cab = new CharAndBits(rotateDecoded[row]);
                    break;
                case BOTH_BACK:
                    if(row >= bothDecoded.length)
                        break;
                    cab = new CharAndBits(bothDecoded[row]);
                    break;
            }
            // common formatting the binary string followed by the printable version of the char
            // unless not defined yet due to the way the GUI refresh the JTable
            if(cab == null)
                return "";
            str = cab.toBinaryString() + " - " + cab.getPrintableChar();
            return str;
        }

    }

}