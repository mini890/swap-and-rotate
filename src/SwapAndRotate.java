import java.util.Scanner;

public class SwapAndRotate {

    public static final int SWAP = 1, ROTATE = 2, BOTH = 3;

    private int[] keySwap1, keySwap2;
    private int[] keyRotate;

    public SwapAndRotate(String swap, String rotate) {
        setKeySwap(swap);
        setKeyRotate(rotate);
    }

    public void setKeySwap(String key) {

        if (!bValidateSwapKey(key)) {
            keySwap1 = new int[0];
            return;
        }

        int len = key.length();
        char[] digit = key.toCharArray();

        keySwap1 = new int[len / 2];
        keySwap2 = new int[len / 2];
        int k = 0;
        for (int i = 0; i < len; i += 2) {
            keySwap1[k] = digit[i] - '0';
            keySwap2[k] = digit[i + 1] - '0';

            keySwap1[k] = 7 - keySwap1[k];
            keySwap2[k] = 7 - keySwap2[k];
            k++;
        }
    }

    public static boolean bValidateSwapKey(String key) {

        if (key == null || key.length() == 0) {
            return false;
        }

        int len = key.length();
        if (len % 2 != 0) {
            return false;
        }

        char[] digit = key.toCharArray();
        for (char c : digit) {
            if (c < '0' || c > '7') {
                return false;
            }
        }

        return true;
    }

    public void setKeyRotate(String key) {
        if (!bValidateRotateKey(key)) {
            keyRotate = new int[0];
            return;
        }

        String[] str = key.split(",");
        int len = str.length;

        keyRotate = new int[len];
        for (int i = 0; i < len; i++)
            keyRotate[i] = Integer.parseInt(str[i].trim());
    }

    public static boolean bValidateRotateKey(String key) {
        if (key == null || key.length() == 0) {
            return false;
        }

        String[] str = key.split(",");
        int len = str.length;

        for (int i = 0; i < len; i++) {
            try {
                Integer.parseInt(str[i].trim());
            } catch (NumberFormatException e) {
                return false;
            }
        }

        return true;
    }

    public char[] encode(String clear, int type) {
        if (clear == null || clear.length() == 0)
            return new char[0];
        switch (type) {
            case SWAP:
                if (keySwap1.length == 0)
                    return new char[0];
                break;
            case ROTATE:
                if (keyRotate.length == 0)
                    return new char[0];
                break;
            case BOTH:
                if (keySwap1.length == 0 || keyRotate.length == 0)
                    return new char[0];
                break;
            default:
                return new char[0];
        }

        CharAndBits[] ea = CharAndBits.newCharAndBitsArray(clear);

        char[] digit = new char[clear.length()];

        if (type == SWAP || type == BOTH) {
            performSwap(ea, digit);
            if (type == BOTH) {
                for (int i = 0; i < ea.length; i++)
                    ea[i] = new CharAndBits(digit[i]);
            }
        }

        if (type == ROTATE || type == BOTH) {
            int index = clear.length() % keyRotate.length;
            int nbRotate = keyRotate[index];

            nbRotate %= ea.length;
            performRotate(ea, digit, nbRotate);
        }
        return digit;
    }

    public char[] decode(String coded, int type) {

        if (coded == null || coded.length() == 0)

            return new char[0];

        switch (type) {

            case SWAP:

                if (keySwap1.length == 0)

                    return new char[0];

                break;

            case ROTATE:

                if (keyRotate.length == 0)
                    return new char[0];
                break;

            case BOTH:

                if (keySwap1.length == 0 || keyRotate.length == 0)

                    return new char[0];

                break;

            default:
                return new char[0];
        }
        CharAndBits[] ea = CharAndBits.newCharAndBitsArray(coded);
        char[] digit = new char[coded.length()];

        if (type == ROTATE || type == BOTH) {

            int index = coded.length() % keyRotate.length;

            int nbRotate = keyRotate[index];

            nbRotate %= ea.length;
            performRotate(ea, digit, -nbRotate);

            if (type == BOTH) {
                for (int i = 0; i < ea.length; i++)
                    ea[i] = new CharAndBits(digit[i]);
            }
        }
        if (type == SWAP || type == BOTH)
            performSwap(ea, digit);
        return digit;
    }

    private void performSwap(CharAndBits[] ea, char[] digit) {
        // pass through all our EasyCharacter
        for (int i = 0; i < ea.length; i++) {
            // get the bit to swap
            int index = i % keySwap1.length;
            int from = keySwap1[index];
            int to = keySwap2[index];
            // get the bit representation of our EasyCharacter
            char[] zeroAndOne = ea[i].getBinaryChar();
            // save first char
            char tmp = zeroAndOne[from];
            // put second at its place
            zeroAndOne[from] = zeroAndOne[to];
            // put back the saved one
            zeroAndOne[to] = tmp;
            // use the Integer parseInt() method with the base specified to read
            // the binary digit
            int newVal = Integer.parseInt(new String(zeroAndOne), 2);
            // and store it in the encoded/decoded digit
            digit[i] = (char) newVal;
        }
    }

    /**
     * Perform the rotate for encoding or decoding Encoding will rotate right
     * decoding left the sign of number to shift received as parameter will tell
     * me wich side to rotate
     */
    private void performRotate(CharAndBits[] ea, char[] digit, int nbRotate) {
        // build an array of char containing the bit representation of the whole
        // message
        // we are using a StringBuilder to perform this operation
        StringBuilder sb = new StringBuilder(ea.length);
        for (CharAndBits c : ea)
            sb.append(c.toBinaryString());
        // make the char[] array
        char[] orig = sb.toString().toCharArray();

        // create the destination array
        char[] dest = new char[orig.length];

        // encoding
        if (nbRotate > 0) {
            // were we will start to copy
            int where = nbRotate;
            // pass through all the bits represented by a char '0' or '1'
            for (char c : orig) {
                // wrap around
                if (where >= dest.length)
                    where = 0;
                // put the char at that position
                dest[where++] = c;
            }
        } else { // decoding
            int from = -nbRotate; // + because nbRotate is negative
            for (int i = 0; i < dest.length; i++) {
                // wrap around
                if (from >= dest.length)
                    from = 0;
                // put the char at that position
                dest[i] = orig[from++];
            }
        }

        // now read back 8 bits by 8 bits the new bits rotated
        int k = 0; // index in digit[] array
        for (int i = 0; i < dest.length; i += 8) {
            String heightBits = new String(dest, i, 8); // build String of 8
            // char '0' or '1'
            int value = Integer.parseInt(heightBits, 2);
            digit[k++] = (char) value;
        }
    }

    /**
     * To test the class
     */
    public static void main(String[] args) {

        // -------------------- generic test ----------------------------------
        // ----------- these lines can be removed in production ---------------

        // the 2 keys
        String swapKey = "021324";
        String rotateKey = "-1,10,23,-14"; // will garanty a rotate of 2
        // create our SwapAndRotate object
        SwapAndRotate sar = new SwapAndRotate(swapKey, rotateKey);
        String msg = "DreamInCode";
        char[] digit, digitBack;
        String decoded;

        // SWAP test
        // print the message and its binary representation
        System.out.println("SWAP test Message: " + msg);
        System.out.println(CharAndBits.toBinaryString(msg.toCharArray()));

        // encode with Swap
        digit = sar.encode(msg, SwapAndRotate.SWAP);
        // print the swap version
        System.out.println("Swap:");
        System.out.println(CharAndBits.toBinaryString(digit));

        // decode message back
        digitBack = sar.decode(new String(digit), SwapAndRotate.SWAP);
        // print back the decoded version in binary
        System.out.println("Swap Back:");
        System.out.println(CharAndBits.toBinaryString(digitBack));

        // message back
        decoded = new String(digitBack);
        System.out.println("Decoded: \"" + decoded + "\" is it the same as \""
                + msg + "\" ? : " + decoded.equals(msg));
        System.out.println();

        // ROTATE test
        // print the message and its binary representation
        System.out.println("ROTATE test Message: " + msg);
        System.out.println(CharAndBits.toBinaryString(msg.toCharArray()));

        // encode with Rotate
        digit = sar.encode(msg, SwapAndRotate.ROTATE);
        // print the Rotate version
        System.out.println("Rotated:");
        System.out.println(CharAndBits.toBinaryString(digit));

        // decode message back
        digitBack = sar.decode(new String(digit), SwapAndRotate.ROTATE);
        // print back the decoded version in binary
        System.out.println("Rotated Back:");
        System.out.println(CharAndBits.toBinaryString(digitBack));

        // message back
        decoded = new String(digitBack);
        System.out.println("Decoded: \"" + decoded + "\" is it the same as \""
                + msg + "\" ? : " + decoded.equals(msg));
        System.out.println();

        // BOTH test
        // print the message and its binary representation
        System.out.println("BOTH test Message: " + msg);
        System.out.println(CharAndBits.toBinaryString(msg.toCharArray()));

        // encode with Rotate
        digit = sar.encode(msg, SwapAndRotate.BOTH);
        // print the Rotate version
        System.out.println("Swapped & Rotated:");
        System.out.println(CharAndBits.toBinaryString(digit));

        // decode message back
        digitBack = sar.decode(new String(digit), SwapAndRotate.BOTH);
        // print back the decoded version in binary
        System.out.println("Swap & Rotated Back:");
        System.out.println(CharAndBits.toBinaryString(digitBack));

        // message back
        decoded = new String(digitBack);
        System.out.println("Decoded: \"" + decoded + "\" is it the same as \""
                + msg + "\" ? : " + decoded.equals(msg));
        System.out.println();

        // --------------- end of generic test --------------------------------

        // user input
        Scanner scan = new Scanner(System.in);
        // prompt for the key
        String keySwap;
        System.out
                .println("Enter SWAP key composed of pairs of bit number from 0 to 7");
        do {
            System.out.print("Enter key like 123464: ");
            keySwap = scan.nextLine();
        } while (!SwapAndRotate.bValidateSwapKey(keySwap));

        // prompt for the Rotate key
        String keyRotate;
        System.out
                .println("Enter ROTATE key composed of integer separated by ,");
        do {
            System.out.print("Enter key like 12,546,323: ");
            keyRotate = scan.nextLine();
        } while (!SwapAndRotate.bValidateRotateKey(keyRotate));

        // create SwapAndRotate object
        SwapAndRotate obj = new SwapAndRotate(keySwap, keyRotate);
        // get message
        System.out.print("Enter message to encode/decode:");
        String message = scan.nextLine();

        // test SWAP
        // print message with bits
        System.out.println("SWAP Original message is: " + message);
        System.out.println(CharAndBits.toBinaryString(msg.toCharArray()));

        char[] swapDigit = obj.encode(message, SwapAndRotate.SWAP);
        System.out.println("After SWAP:");
        System.out.println(CharAndBits.toBinaryString(swapDigit));
        char[] swapBack = obj.decode(new String(swapDigit), SwapAndRotate.SWAP);
        System.out.println("After decode:");
        System.out.println(CharAndBits.toBinaryString(swapBack));
        System.out.println("Decoded message: " + new String(swapBack));
        System.out.println();

        // test ROTATE
        // print message with bits
        System.out.println("ROTATE Original message is: " + message);
        System.out.println(CharAndBits.toBinaryString(msg.toCharArray()));

        char[] rotateDigit = obj.encode(message, SwapAndRotate.ROTATE);
        System.out.println("After ROTATE:");
        System.out.println(CharAndBits.toBinaryString(rotateDigit));
        char[] rotateBack = obj.decode(new String(rotateDigit),
                SwapAndRotate.ROTATE);
        System.out.println("After decode:");
        System.out.println(CharAndBits.toBinaryString(rotateBack));
        System.out.println("Decoded message: " + new String(rotateBack));
        System.out.println();

        // test BOTH
        // print message with bits
        System.out.println("BOTH Original message is: " + message);
        System.out.println(CharAndBits.toBinaryString(msg.toCharArray()));

        char[] bothDigit = obj.encode(message, SwapAndRotate.BOTH);
        System.out.println("After BOTH:");
        System.out.println(CharAndBits.toBinaryString(bothDigit));
        char[] bothBack = obj.decode(new String(bothDigit), SwapAndRotate.BOTH);
        System.out.println("After decode:");
        System.out.println(CharAndBits.toBinaryString(bothBack));
        System.out.println("Decoded message: " + new String(bothBack));
        System.out.println();
        scan.close();
    }
}